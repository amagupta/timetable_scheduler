#include "TimeTable.h"

void mapSID(map<string, string> &stu_course) {
    ifstream fp;
    map<string, string>::iterator it;
    string SID, courses;
    fp.open("processedData/SID_CID.csv");
    while (!fp.eof()) {
        fp >> SID >> courses;
        it = stu_course.find(SID);
        if (it == stu_course.end()) {
            stu_course[SID] = courses;
        }
        else {
            stu_course[SID].append(":");
            stu_course[SID].append(courses);
        }
    }
    fp.close();
}

void mapTimeTable(map<string, string> &tt) {
    ifstream tt_info;
    string key1, key2;
    string time, value;
    tt_info.open("processedData/slot_time_avail.csv");
    while (!tt_info.eof()) {
        tt_info >> key1 >> key2 >> time >> value;
        if (key1.size() == 1) {
            key1.append(key2);
            tt[key1] = value;
        }
    }
    tt_info.close();
}

void mapFID(map<string, struct faculty> &faculty_map) {
    ifstream faculty_info;

    faculty_info.open("processedData/courseInfo.csv");
    
    faculty init;
    init.classes_handled = 0;
    string f1, f2, f3, facultyID;

    while (!faculty_info.eof()) {
        faculty_info >> f1 >> f2 >> f3 >> facultyID;
        init.FID = facultyID;
        faculty_map[facultyID] = init;
    }
    faculty_info.close();

    map<string, struct faculty>::iterator it;
    map<string, string> days;
    days["Monday"]      = "0";
    days["Tuesday"]     = "1";
    days["Wednesday"]   = "2";
    days["Thursday"]    = "3";
    days["Friday"]      = "4";
    days["Saturday"]    = "5";
    days["Sunday"]      = "6";
    faculty object;
    string day;
    string slot;
    string final;
    string FID;
    faculty_info.open("processedData/facUnvail.csv");
    while (!faculty_info.eof()) {
        faculty_info >> FID;
        faculty_info >> day;
        faculty_info >> slot;
        it = faculty_map.find(FID);
        if (it != faculty_map.end()) {
            final = (it->second).time_unavailable;
            final.append(days[day]);
            final.append(slot);
            final.append(":");
            (it->second).time_unavailable = final;
            (it->second).classes_handled = 0;
        }
        else {
            object.FID = FID;
            final.append(days[day]);
            final.append(slot);
            final.append(":");
            object.time_unavailable = final;
            object.classes_handled = 0;
            faculty_map[FID] = object;
        }
    }
    faculty_info.close();
}

void mapCID(map<string, struct course> &course_map) {
    ifstream course_info;
    string CID;
    int num_batches;
    course object;
    course_info.open("processedData/courseInfo.csv");
    while (!course_info.eof()) {
        course_info >> CID;
        object.CID = CID;
        course_info >> object.num_lec;
        course_info >> object.num_prac;
        course_info >> object.FID;
        object.SID = getStudents(CID);
        object.num_batches = getBatches(CID);
        course_map[CID] = object;
    }
    course_info.close();
}

int getBatches(string CID) {
    ifstream fp;
    string course;
    int num_batches;
    fp.open("processedData/CID_batches.csv");
    while (!fp.eof()) {
        fp >> course >> num_batches;
        if (course == CID)
            break;
    }
    fp.close();
    return num_batches;
}

string getStudents(string CID) {
    ifstream CID_SID;
    string course, student;
    string SID;
    CID_SID.open("processedData/CID_SID.csv");
    while (!CID_SID.eof()) {
        CID_SID >> course >> student;
        if (course == CID) {
            SID.append(":");
            SID.append(student);
        }
    }
    if (SID.empty()) {
        SID.append("none");
    }
    CID_SID.close();
    return SID;
}
