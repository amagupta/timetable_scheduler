#include "TimeTable.h"
int NO_OF_DAYS;
int NO_OF_SLOTS;
int atoi(const char *c)
{
		int sum = 0;
		for (int i = 0; c[i]; i++)
				sum += sum * 10 + (c[i] - '0');
		return sum;
}
int main(int argc, char* argv[])
{			
		map <string, string> time_table ;
		map <string, struct faculty> fac;
		map <string, string> student_list;
		map <string, string> batch_TT;
		map <string, string> Student_TT;
        map <string, string> Faculty_TT;

		LIST courses;

		mapFID(fac);
		mapCID(courses);
		mapSID(student_list);
		mapTimeTable(time_table);

		map<string, string>::iterator it = time_table.end();
		it--;
		NO_OF_DAYS = atoi((*it).first.substr(0,1).c_str()) - 1;
		NO_OF_SLOTS = atoi((*it).first.substr(1,1).c_str());

		slotAllocator(time_table, courses, fac);
		string stud_id;
		string batch;
        string fac_id;
		if (argv[1])
		{
				string option = string(argv[1]);
				if (argv[2])
				{
						switch (option[1])
						{
								case 'b' :
										batch = string(argv[2]);
										getBatchTT(batch, student_list, time_table, batch_TT);
										printBatchTT(batch_TT);
										break;
								case 's' :
										stud_id = string(argv[2]);
										getStudentTT(stud_id, time_table, student_list, Student_TT); 
										printStudentTT(Student_TT);
										break;
                                case 'f' :
                                        fac_id = string(argv[2]);
                                        getFacultyTT(fac_id, time_table, courses, Faculty_TT);
                                        printFacultyTT(Faculty_TT);
                                        break;
								default: 
										cout << "Invalid Argument!!\n";
										break;
						}
				}
				else
				{
						cout << "Usage: <-b/-s/-f> <id>\n";
				}
		}
		else
		{
				printTimetable(time_table);
		}
		return 0;
}
