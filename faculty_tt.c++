#include "TimeTable.h"
extern int NO_OF_DAYS;
extern int NO_OF_SLOTS;
void getFacultyTT(string fac_id, map<string, string> Institute_TT, map<string, course> Course_List, map<string, string>& Faculty_TT)
{
    map<string, course>::iterator it;
    vector<string> courses;

    for (it = Course_List.begin(); it != Course_List.end(); it++) {

        if ((it -> second).FID == fac_id) {
            courses.push_back((it -> second).CID);
        }
    }
    
    for (int i = 0; i < NO_OF_DAYS; i++) {
        for (int j =0; j < NO_OF_SLOTS; j++) {
            string slot = toStr(i,j);
            for (int k = 0; k < courses.size(); k++) {
                if (Institute_TT[slot].find(courses[k]) != string::npos) {
                    if (Faculty_TT[slot] != "")
                        Faculty_TT[slot] += ":";
                    Faculty_TT[slot] += courses[k];
                }
            }
        }
    }
}

void printFacultyTT(map<string, string> Faculty_TT) {
    string days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    for (int i =0 ; i < NO_OF_SLOTS; i++)
        cout << "\t" << i+1;
    cout << endl;
    for (int i = 0; i < NO_OF_DAYS; i++) {
        cout << days[i] << "\t";
        for (int j = 0; j < NO_OF_SLOTS; j++) {
            string slot = toStr(i,j);
            cout << Faculty_TT[slot] << "\t";
        }
        cout << endl;
    }
}
