#include "TimeTable.h"
//#define traverse(c, i) for (typeof((c).begin()) i = (c).begin(); i != (c).end(); i++)

extern int NO_OF_DAYS;
extern int NO_OF_SLOTS;

void getBatchTT(string batch, map <string, string> student_list, map <string, string> Institute_TT, map <string, string>& Batch_TT)
{
    set <string> courses;
    string c;
    traverse(student_list, it)
    {
        if (it->first.find(batch) != string::npos)
        {
            stringstream ss(it->second);
            while (getline(ss, c, ':')) 
            {
                courses.insert(c);
            }
        }
    }
    for (int i = 0; i < NO_OF_DAYS; i++) {
        for (int j =0; j < NO_OF_SLOTS; j++) {
            string slot = toStr(i,j);
            traverse(courses, it)
            {
                if (Institute_TT[slot].find(*it) != string::npos) {
                    if (Batch_TT[slot] != "")
                        Batch_TT[slot] += ":";
                    Batch_TT[slot] += *it;
                }
            }
        }
    }
}

void printBatchTT(map<string, string> Batch_TT) {
    string days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    for (int i = 0; i < NO_OF_SLOTS; i++)
        cout << "\t" << i+1;
    cout << endl;
    for (int i = 0; i < NO_OF_DAYS; i++) {
        cout << days[i]<< "\t";
        for (int j = 0; j < NO_OF_SLOTS; j++) {
            string slot = toStr(i,j);
            cout << Batch_TT[slot] << "\t";
        }
        cout << endl;
    }
}
