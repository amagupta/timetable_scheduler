#include "TimeTable.h"
extern int NO_OF_DAYS;
extern int NO_OF_SLOTS;
void getStudentTT(string stud_id, map<string, string> Institute_TT, map<string, string>& Student_List, map<string, string>& Student_TT)
{
    string courses = Student_List[stud_id];
    stringstream ss(courses);
    string c;
    vector<string> course_list;
    while (getline(ss, c, ':')) {
        course_list.push_back(c);
    }

    for (int i = 0; i < NO_OF_DAYS; i++) {
        for (int j =0; j < NO_OF_SLOTS; j++) {
            string slot = toStr(i,j);
            for (int k = 0; k < course_list.size(); k++) {
                if (Institute_TT[slot].find(course_list[k]) != string::npos) {
                    Student_TT[slot] = course_list[k];
                }
            }
        }
    }
}

void printStudentTT(map<string, string> Student_TT) {
    string days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    for (int i = 0; i < NO_OF_SLOTS; i++) {
        cout << "\t" << i+1;
    }
    cout << endl;
    for (int i = 0; i < NO_OF_DAYS; i++) {
        cout << days[i] << "\t";
        for (int j = 0; j < NO_OF_SLOTS; j++) {
            string slot = toStr(i,j);
            cout << Student_TT[slot] << "\t";
        }
        
        cout << endl;
    }
}
