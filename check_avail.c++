#include "TimeTable.h"

map<string, pair<string,string> > already_avail;

bool hasCollision(string slot, course c) 
{
	if ( isSlotEmpty(slot) ) {
        insertIntoMap ( slot,c,1);
	}

	else {
		if ( checkCollision ( slot,c) ) {
			return true;
		}
		else {
			insertIntoMap (slot,c,0);
		} 
    }
        map <string, pair<string,string> > :: iterator pos;

      
    return false;
}
bool checkCollision (string slot, course cr) {
	string current_faculty = already_avail[slot].first;
	string current_students= already_avail[slot].second;
	string students = cr.SID;
	string faculty = cr.FID;
	string student_1;
	stringstream stream(students);
	while ( getline(stream, student_1,':' ) ) {   
		if ( int( current_students.find(student_1)) > 0)
        {
			return true;
		}
		student_1 = "";
	}
	string faculty_1;
	stringstream stream1(faculty);
	while ( getline(stream1, faculty_1, ':' )) {
		if ( int( current_faculty.find(faculty_1)) > 0)
        {
            return true;
		}
		faculty_1 = "";

	}
	return false;
}


void insertIntoMap (string slot, course cr ,int First_time) {
	if(First_time) {
		already_avail.insert( std::make_pair (slot,make_pair(cr.FID, cr.SID) ) );
	}

	else {
		already_avail[slot].first += ":"+ cr.FID;
		already_avail[slot].second +=":"+ cr.SID;
	}
}

bool isSlotEmpty (string slot){

	int found = 0;
	map <string, pair<string,string> > :: iterator pos;
	for (pos = already_avail.begin(); pos != already_avail.end(); ++pos) {
		if ( pos->first == slot ) {
			found++;
		}
	}

	if(found == 0) {
		return true;
	}
	else {
		return false;
	}

}
