#include "TimeTable.h"
extern int NO_OF_DAYS;
extern int NO_OF_SLOTS;
void slotAllocator(map<string, string> &time_table, LIST &courses, map <string, struct faculty> &staff_list)
{

	vector<string> lec;
	vector<string> prac;

	for (int i = 0; i < NO_OF_DAYS; i++)
	{
		vector<string> unassigned_courses = getCourseList(courses);

		pair<vector<string>, vector<string> > lec_prac = splitCourse(unassigned_courses, courses);
		lec = lec_prac.first;
		prac = lec_prac.second;
		string slot;

		if (lec.size() == 0 && prac.size() == 0) 
			return;

		for (int j = 0; j < NO_OF_SLOTS - 2; j++)
		{
			slot = toStr(i, j);

			if(lec.size() == 0) break;

			if (time_table[slot] == "U")
				continue;
			else 
				time_table[slot] = "";

			assignSlot(slot, lec, time_table, courses, staff_list);
		}

		slot = toStr(i, NO_OF_SLOTS - 2);
		time_table[slot] = "";
		allocatePracticals(time_table, courses, staff_list, slot, prac);
		refreshFaculty(staff_list);
	}
	if (lec.size() != 0) {
		compromiseConstraint(time_table, lec, courses, staff_list);
		cout << lec.size() << " " << prac.size() << "courses not filled" << endl;
	}
	for (int i = 0; i < lec.size(); i++)
		cout << lec[i] << " ";
}

void assignSlot(string slot, vector<string> &lec, map<string, string> & time_table, LIST &courses, map <string, faculty> &staff_list)
{

	vector<string>::iterator it = lec.begin();
	for (; it != lec.end(); it++)
	{
		string hall = "L1:";
		string course = (*it);
		if (canBeAssigned(slot, time_table, courses, staff_list, hall, course))
		{
			lec.erase(it);
			break;
		}
	}

	for (int k = 1; k < NO_OF_HALLS; k++)
	{
		vector<string>::iterator it = lec.end();
		it--;
		for (; it >= lec.begin(); it--)
		{
			string hall = "::L" + toString(k + 1) + ":";
			string course = (*it);
			if (canBeAssigned(slot, time_table, courses, staff_list, hall, course))
			{
				lec.erase(it);
				break;
			}
		}
	}
}

bool canBeAssigned(string slot, map<string, string>&time_table, LIST &courses, map <string, faculty> &staff_list, string hall, string course)
{
	string fid = courses[course].FID;
	if (isFacultyAvailable(fid, slot, staff_list))
	{
		if (staff_list[fid].classes_handled < FACULTY_CONSTRAINT)
		{
			if (!hasCollision(slot, courses[course]) )
			{
				time_table[slot] += hall + course;	
				update(courses, course);
				staff_list[fid].classes_handled++;
				return true;
			}
		}
	}
	return false;
}

void allocatePracticals(map<string, string> &time_table, LIST &courses, map <string, struct faculty> &staff_list, string slot, vector<string> &prac)
{
	for (int k = 0; k < NO_OF_LABS; k++)
	{
		if(prac.size() == 0) break;

		for (int l = 0; l < prac.size(); l++)
		{
			string fid = courses[prac[l]].FID;

			if(isFacultyAvailable(fid, slot, staff_list))
			{
				if (!hasCollision(slot, courses[prac[l]]))
				{
					if (k)
						time_table[slot] += "::";
					time_table[slot] += prac[l];
					prac.erase(prac.begin() + l);
					courses[prac[l]].num_prac--;
					break;
				}
			}
		}
	}

}

void update(LIST &courses, string cur_course)
{
	courses[cur_course].num_lec--;
}

bool isFacultyAvailable(string fid, string slot,  map <string, struct faculty> staff_list)
{
	string unavail = staff_list[fid].time_unavailable;
	if (unavail.find(slot) != string::npos)
		return false;
	return true;
}

void refreshFaculty(map <string, struct faculty> &faculty)
{
	traverse(faculty, it)
	{
		it->second.classes_handled = 0;
	}
}

pair<vector<string>, vector<string> >splitCourse(vector<string> unassigned_courses, LIST courses)
{
	pair<vector<string>, vector<string> > lec_prac;
	vector<string>lec;
	vector<string>prac;
	for (int i = 0; i < unassigned_courses.size(); i++)
	{
		if (courses[unassigned_courses[i]].num_lec > 0 )
			lec.push_back(unassigned_courses[i]);

		if (courses[unassigned_courses[i]].num_prac > 0 )
			prac.push_back(unassigned_courses[i]);
	}
	lec_prac.first = lec;
	lec_prac.second = prac;
	return lec_prac;
}

string toStr(int x, int y)
{
	stringstream ss;
	string s;
	ss << x;
	ss << y;
	ss >> s;
	return s;
}

string toString(int x)
{
	stringstream ss;
	ss << x;
	return ss.str();
}
vector<string> parse(string s)
{
	vector <string> v;
	int pos;
	while ((pos = s.find("::")) != string::npos)
	{
		v.push_back(s.substr(0, pos));
		s = s.substr(pos + 2, s.size());
	}
	v.push_back(s);
	return v;
}
void printTimetable(map <string, string> time_table)
{
	string days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
	map<string, string>::iterator it = time_table.end();
	it--;
	vector <vector<string> > Day_TT(NO_OF_LABS + 1, vector<string>(NO_OF_SLOTS + 1, ""));
	map <int, vector<vector<string> > > TT;
	for (int i = 0; i < NO_OF_DAYS; i++) {
		for (int j = 0; j < NO_OF_SLOTS - 1; j++)
		{
			string slot = toStr(i, j);
			if (time_table[slot] == "U") continue;
			vector <string> parsed_course = parse(time_table[slot]);
			for (int k = 0; k < parsed_course.size(); k++)
			{
				Day_TT[k][j] = parsed_course[k];
			}

		}
		TT[i] = Day_TT;
		for (int j = 0; j < NO_OF_SLOTS - 1; j++)
		{
			for (int k = 0; k < Day_TT.size(); k++)
			{
				Day_TT[k][j] = ""; 
			}

		}
	}

	cout << endl;
	for (int i =0 ; i < NO_OF_SLOTS - 1; i++)
		cout << "\t    " << i+1 << "\t";
	cout << "\n\n";

	for (int i = 0; i < NO_OF_DAYS; i++) 
	{
		cout << days[i] ;
		for (int k = 0; k < TT[i].size(); k++)
		{
			cout << "\t    ";
			for (int j = 0; j < NO_OF_SLOTS; j++)
			{
				if(TT[i][k][j] == "" || TT[i][k][j] == "A")cout << "\t     \t";
				else
					cout << TT[i][k][j] << "\t";
			}
			cout << endl;
		}
		cout << endl;
	}
	cout << endl;			
}
