#include "TimeTable.h"

vector<string> getCourseList(map<string, course>& cid2course) {

    map<string, course>::iterator cid;

    multimap<int, course> priorityMap;
    multimap<int, course>::iterator it;

    vector<string> course_priority;
    
    int key;

    for (cid = cid2course.begin(); cid != cid2course.end(); cid++) {

        if (((*cid).second.num_lec > 0 || (*cid).second.num_prac > 0) && (*cid).second.SID != "none") {

            key = (*cid).second.num_batches;
            priorityMap.insert(pair<int, course> (key, (*cid).second));

        }
    }

    int prev = 0;
    multimap<int, string> lecPracPriority;
    multimap<int, string>::iterator lecPracIt;

    for (it = priorityMap.begin(); it != priorityMap.end(); it++) {

        if (prev != (*it).first) {
            prev = (*it).first;
            for (lecPracIt = lecPracPriority.begin(); lecPracIt != lecPracPriority.end(); lecPracIt++) {
                course_priority.insert(course_priority.begin(), (*lecPracIt).second);
            }
            lecPracPriority.clear();
        }

        lecPracPriority.insert(pair<int, string> ((*it).second.num_lec + (*it).second.num_prac, (*it).second.CID));
    }

    for (lecPracIt = lecPracPriority.begin(); lecPracIt != lecPracPriority.end(); lecPracIt++) {
        course_priority.insert(course_priority.begin(), (*lecPracIt).second);
    }
 
    return course_priority;
}

void testGetCourseList() {

    map<string, course> testMap;
    string line;

    mapCID(testMap);

    vector<string> courseList = getCourseList(testMap);

    for (int i = 0; i < courseList.size(); i++)
        cout << courseList[i] << endl;

}
