#include "TimeTable.h"
extern int NO_OF_DAYS;
extern int NO_OF_SLOTS;
void assignSlot_compromise(string slot, vector<string> &lec, map<string, string> & time_table, LIST &courses, map <string, struct faculty> staff_list, map<string, bool> compromise_map)
{

	vector<string>::iterator it = lec.begin();
	for (; it != lec.end(); it++)
	{
		string fid = courses[*it].FID;
		if (isFacultyAvailable(fid, slot, staff_list))
		{
			if ((staff_list[fid].classes_handled < FACULTY_CONSTRAINT) && (compromise_map["fac_constraint"]))
			{
				if (!hasCollision(slot, courses[*it]) )
				{
					time_table[slot] += (*it);	
					update(courses, (*it));
					if (courses[(*it)].num_lec == 0 || (!(compromise_map["num_lectures"]))) 
					{
						lec.erase(it);
					}
					break;
				}
			}
		}
	}

	for (int k = 1; k < NO_OF_HALLS; k++)
	{
		vector<string>::iterator it = lec.end();
		it--;
		for (; it >= lec.begin(); it--)
		{
			string fid = courses[(*it)].FID;
			if (isFacultyAvailable(fid, slot, staff_list))
			{
				if (staff_list[fid].classes_handled < FACULTY_CONSTRAINT && (compromise_map["fac_constraint"]))
				{
					if (!hasCollision(slot, courses[(*it)]))
					{
						time_table[slot] += "::";
						time_table[slot] += (*it); 	
						update(courses, (*it));
						if (courses[(*it)].num_lec == 0 ||(!(compromise_map["num_lectures"]))) 
						{
							lec.erase(it);
						}
						break;
					}
				}
			}
		}
	}
}

void compromiseConstraint(map<string, string>& time_table, vector<string>& lec, map<string, struct course>& courses, map<string, struct faculty>& staff_list)
{

	map<string, bool> compromise_map;
	compromise_map["fac_constraint"] = true;
	compromise_map["num_lectures"] = false;

	for (int i = 0; i < NO_OF_DAYS; i++)
	{
		string slot;
		if (lec.size() == 0) 
			return;
		for (int j = 0; j < NO_OF_SLOTS - 2; j++)
		{
			slot = toStr(i, j);
			if(lec.size() == 0) break;
			assignSlot_compromise(slot, lec, time_table, courses, staff_list, compromise_map);
		}
	}

	if (lec.size() != 0) 
	{
		cout << "Still unallocated Courses";
	}
}

