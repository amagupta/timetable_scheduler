#include <iostream>
#include <map>
#include <vector>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#define NO_OF_HALLS 3
#define NO_OF_LABS 5
#define FACULTY_CONSTRAINT 3
#define LIST map<string, struct course>
#define traverse(c, i) for (typeof((c).begin()) i = (c).begin(); i != (c).end(); i++)

using namespace std;

struct course {
	string CID;
	string FID;
	string SID;
	int num_lec;
	int num_prac;
	int num_batches;
};

struct faculty {
	string FID;
	string time_unavailable;
	int classes_handled;
};
int atoi(const char *c);

bool hasCollision(string slot, course c);
bool isSlotEmpty (string slot);
void insertIntoMap (string slot, course cr ,int First_time) ;
bool checkCollision (string slot, course cr) ;

vector<string> getCourseList(map<string, course>&);
void testGetCourseList();

string getStudents(string);
int getBatches(string);
void mapCID(map<string, struct course> &); 
void mapFID(map<string, struct faculty> &);
void mapSID(map<string, string>&); 
void mapTimeTable(map<string, string> &); 

pair<vector<string>, vector<string> > splitCourse(vector<string>, map<string, struct course>);
string toStr(int, int);
string toString(int);
void update(map<string, struct course>&, string);
bool isFacultyAvailable(string, string,  map <string, struct faculty>);
void assignSlot(string, vector<string> &, map<string, string>&, map<string, course>&, map <string, faculty>&);
void refreshFaculty(map <string, struct faculty> &);
void slotAllocator(map<string, string> &, map<string, struct course>&, map <string, struct faculty>&);
void allocatePracticals(map<string, string> &, LIST &, map <string, struct faculty> &, string, vector<string> &);
bool canBeAssigned(string , map<string, string>&, LIST &, map <string, faculty> &, string, string );


void assignSlot_compromise(string, vector<string> &, map<string, string> &, map<string, course> &, map <string, struct faculty>, map<string, bool>);
void compromiseConstraint(map<string, string> &, vector<string> &, map<string, struct course> &, map<string, struct faculty> &);
void printTimetable(map <string, string>);

void getStudentTT(string, map<string, string>, map<string, string>&, map<string, string>&);
void printStudentTT(map<string, string>);

void getBatchTT(string, map<string, string>, map<string, string>, map<string, string>&);
void printBatchTT(map<string, string>);
 
void getFacultyTT(string, map<string, string>, map<string, course>, map<string, string>&);
void printFacultyTT(map<string, string>);
