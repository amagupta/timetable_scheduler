objfiles = main.o CIDmap.o check_avail.o getPriorityList.o compromiseConstraints.o slot_allocater.o batch_tt.o student_tt.o faculty_tt.o
TimeTable: $(objfiles)
	g++ $(objfiles) -o TimeTable
CIDmap.o: CIDmap.c++ TimeTable.h
	g++ -c CIDmap.c++
check_avail.o: check_avail.c++ TimeTable.h 
	g++ -c check_avail.c++ 
getPriorityList.o: getPriorityList.c++ TimeTable.h
	g++ -c getPriorityList.c++
compromiseConstraints.o: compromiseConstraints.c++ TimeTable.h
	g++ -c compromiseConstraints.c++
slot_allocater.o: slot_allocater.cpp check_avail.c++ getPriorityList.c++ compromiseConstraints.c++ TimeTable.h
	g++ -c slot_allocater.cpp
student_tt.o: CIDmap.c++ slot_allocater.cpp TimeTable.h
	g++ -c student_tt.c++
batch_tt.o: CIDmap.c++ slot_allocater.cpp TimeTable.h
	g++ -c batch_tt.c++
faculty_tt.o: CIDmap.c++ slot_allocater.cpp TimeTable.h
	g++ -c faculty_tt.c++
main.o: main.c++ student_tt.c++ batch_tt.c++ slot_allocater.cpp TimeTable.h
	g++ -c main.c++
clean:
	rm TimeTable $(objfiles)
